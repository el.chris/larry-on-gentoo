#!/bin/bash

VERSION=$1
DTB_FILE="bcm2711-rpi-4-b.dtb"

if [[ -z $VERSION ]]; then
  reboot
fi

mount /boot

if [[ $VERSION = "latest" ]]; then
  echo rebooting into kernel: gentoo-latest
  cp /boot/${DTB_FILE}-latest /boot/${DTB_FILE}
  sed -i 's/kernel=gentoo-stable.img/kernel=gentoo-latest.img/g' /boot/config.txt
elif [[ $VERSION = "stable" ]]; then
  echo rebooting into kernel: gentoo-stable
  cp /boot/${DTB_FILE}-stable /boot/${DTB_FILE}
  sed -i 's/kernel=gentoo-latest.img/kernel=gentoo-stable.img/g' /boot/config.txt
fi

reboot
