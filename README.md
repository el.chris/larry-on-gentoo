# Create bootable SD-Card 

```
    user:       root                        user:       larry
    password:   larry                       password:   larry
```

## Install image
To install the image you can use the `restoreDeviceFromImage.sh` script. Insert
your sd card or usb drive and then take a look with fdisk under which device name 
they are known to your system.

```sh 
sudo fdisk -l

Disk /dev/sdX: 59.48 GiB, 63864569856 bytes, 124735488 sectors
Disk model: STORAGE DEVICE
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x272e23ac

Device     Boot  Start       End   Sectors  Size Id Type
/dev/sdX1         2048    526335    524288  256M  c W95 FAT32 (LBA)
/dev/sdX2       526336 124735487 124209152 59.2G 83 Linux
```

In your case the `X` is replaced with a letter like `a` or `c`. If you have found
the correct device you can restore the image with

```sh
sudo ./restoreDeviceFromImage.sh /dev/sdX image-latest.tar
```

If you would like to be able to boot from usb then you have to add the parameter `usb-boot`
like

```sh
sudo ./restoreDeviceFromImage.sh /dev/sdX image-latest.tar usb-boot
```

## Using Graphical Environment
To use the pre-installed graphical environment login as `larry` with the password `larry`
and then enter the command `startxfce4`.

The `ntp` daemon is not started by default so if you would like to have the current
time available type
```sh
sudo /etc/init.d/ntp-client start
```

### Enable NetworkManager and disable Hostapd
```
rc-update add NetworkManager default 
rc-update del hostapd default
```

# Build System
## Further Information
https://wiki.gentoo.org/wiki/User:NeddySeagoon/Raspberry_Pi4_64_Bit_Install

## Kernel
### Cross Compile

 1. git clone https://github.com/raspberrypi/linux
 2. cd linux
 3. ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make bcm2711_defconfig
 4. ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make menuconfig
    CPU Power Management -> CPU Frequency Scaling -> Default CPUFreq governor (ondemand)
 5. ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make -j16
 6. cp /home/<user>/raspberrypi/linux/arch/arm64/boot/Image /mnt/gentoo/boot/kernel8.img
 7. cp /home/<user>/raspberrypi/linux/arch/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dtb /mnt/gentoo/boot
 8. ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make modules_install INSTALL_MOD_PATH=/mnt/gentoo

### Local

 1. git clone https://github.com/raspberrypi/linux
 2. cd linux
 3. git checkout rpi-5.10.y
 4. make bcm2711_defconfig
 5. make menuconfig
    CPU Power Management -> CPU Frequency Scaling -> Default CPUFreq governor (ondemand)
 6. make -j4
 7. mount /boot
 8. cp arch/arm64/boot/Image /boot/gentoo-latest.img
 9. cp arch/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dtb /boot
 10. make modules_install


## Configure System

 - mkdir /etc/portage/repos.conf
 - cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
 - edit /etc/portage/make.conf
   change:
     CHOST="aarch64-linux-gnu"
   add:
     COMMON_FLAGS="-march=native -O2 -pipe"
     ACCEPT_LICENSE="*"
     MAKEOPTS="-j4"
 
 - date -s "Thu Aug  9 21:31:26 UTC 2012"
 - emerge net-misc/ntp net-misc/dhcpcd sudo
 - rc-update add dhcpcd default
 - rc-update add ntp-client default
 - useradd -m -g users -G wheel,audio,video larry
 - emerge --sync; emerge -uD world
 - echo "Europe/Berlin" > /etc/timezone
 - cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
 - rc-update add swclock boot
 - rc-update del hwclock boot
 - rc-update add sshd default
 - edit /etc/ssh/sshd_config
     PermitRootLogin yes
 
 - emerge linux-firmware
 
 - add genpi64 repo in /usr/local/
     add /etc/portage/repos.conf/genpi64.conf
     ```
     [genpi64]
         location = /usr/local/genpi64
         sync-type = git
         sync-uri = https://github.com/sakaki-/genpi64-overlay.git
         auto-sync = yes
     ```
     cd /usr/local/
     git clone https://github.com/sakaki-/genpi64-overlay.git
 
 - mkdir /etc/portage/package.accept_keywords
     add /etc/portage/package.accept_keywords/raspberrypi
     ```
     media-libs/raspberrypi-userland ~arm64
     dev-embedded/rpi4-eeprom-images ~arm64
     dev-embedded/rpi4-eeprom-updater ~arm64
     sys-apps/flashrom ~arm64
     ```
     emerge raspberrypi-userland rpi4-eeprom-images rpi4-eeprom-updater
 
 - emerge fish
 - add user larry
    ```sh
     useradd -m -g users -G wheel,audio,video -s /bin/fish larry
     passwd larry
    ```
 
 - add system groups spi, i2c, gpio, plugdev
    ```sh
     groupadd spi # and the following: i2c, gpio, plugdev
     usermod -a -G gpio,spio,i2c,plugdev larry
    ```

 - add /etc/udev/rules.d/10-larry.conf
     ```
     SUBSYSTEM=="input", GROUP="input", MODE="0660"
     SUBSYSTEM=="i2c-dev", GROUP="i2c", MODE="0660"
     SUBSYSTEM=="spidev", GROUP="spi", MODE="0660"
     SUBSYSTEM=="bcm2835-gpiomem", GROUP="gpio", MODE="0660"
     ```

# Update Repo with new image

