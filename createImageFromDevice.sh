#!/bin/bash

STORE=$(pwd)
BOOT_FILE=boot.tar.xz
ROOT_FILE=root.tar.xz
DEVICE=$1

TAR_ARGS="cfJp"

if [[ $(whoami) != "root" ]]; then 
    echo "you must be root to create an image"
    exit
fi

if [[ -z $DEVICE ]]; then
    echo "First argument has to be the device where you would like to create a backup from."
    echo "Like /dev/sdc"
    exit
fi

echo -n "Creating boot image : "
mount ${DEVICE}1 /mnt
cd /mnt
tar ${TAR_ARGS} ${STORE}/${BOOT_FILE} *
cd /
umount /mnt
echo "done"


echo -n "Creating root image : "
e2fsck -fy ${DEVICE}2
mount ${DEVICE}2 /mnt
cd /mnt
tar ${TAR_ARGS} ${STORE}/${ROOT_FILE} \
        --exclude='home/larry/Development' \
        --exclude='home/larry/.ssh' \
        --exclude='root/.ssh' \
        --exclude='usr/portage/distfiles/*' \
        --exclude='var/cache/distfiles/*' \
        --exclude='tmp/*' \
        --exclude='lost+found/*' \
        *
cd /
umount /mnt
echo "done"

echo -n "Creating disk image : "
cd ${STORE}
tar cf image_$(date +%Y-%m-%d_%H.%M.%S).tar ${BOOT_FILE} ${ROOT_FILE}
rm ${BOOT_FILE}
rm ${ROOT_FILE}
echo "done"
